const sqlite3 = require('sqlite3').verbose();
const bd = new sqlite3.Database('data.db');

const FATURAS_SCHEMA = `
CREATE TABLE IF NOT EXISTS FATURAS (
    idfatura INTEGER PRIMARY KEY AUTOINCREMENT, 
    idusuario integer,
    nome_empresa VARCHAR(255) NOT NULL,
    valor REAL NOT NULL,
    data_vencimento VARCHAR(40) NOT NULL,
    pagou VARCHAR(3) NOT NULL
)
`;

const INSERIR_FATURA_1 = 
`
INSERT INTO faturas (
    idusuario,
    nome_empresa,
    valor,
    data_vencimento,
    pagou
) Values (1, 'Automasoft LTDA', 256.20, '20/04/2019', 'SIM'); 
`;

bd.serialize(() => {
    bd.run("PRAGMA foreign_keys=ON");
    bd.run(FATURAS_SCHEMA);
    bd.run(INSERIR_FATURA_1);

    bd.each("SELECT * FROM FATURAS", (err, usuario) => {
        console.log('Fatura: ');
        console.log(usuario);
    });
});

process.on('SIGINT', () =>
    bd.close(() => {
        console.log('BD encerrado!');
        process.exit(0);
    })
);

module.exports = bd;