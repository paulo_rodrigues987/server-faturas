let tabelaFaturas = document.querySelector('#faturas');
tabelaFaturas.addEventListener('click', (evento) => {
    let elementoClicado = evento.target;

    if (elementoClicado.dataset.type == 'remocao') {
        let faturasidfatura = elementoClicado.dataset.ref;
        fetch(`http://localhost:3000/faturas/${faturasidfatura}`, { method: 'DELETE' })
            .then(resposta => {

                let tr = elementoClicado.closest(`#faturas_${faturasidfatura}`);
                tr.remove();
            })
            .catch(erro => console.log(erro));
    }
});