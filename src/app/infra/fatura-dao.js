class FaturaDao{

    constructor(db) {
        this._db = db;
    }

    adiciona(fatura) {
        return new Promise((resolve, reject) => {
            this._db.run(`
                INSERT INTO FATURAS (
                    idusuario, 
                    nome_empresa,
                    valor,
                    data_vencimento,
                    pagou
                ) values (?,?,?,?,?)
                `,
                [
                    fatura.idusuario,
                    fatura.nome_empresa,
                    fatura.valor,
                    fatura.data_vencimento,
                    fatura.pagou
                ],
                function (err) {
                    if (err) {
                        console.log(err);
                        return reject('Não foi possível adicionar a fatura!');
                    }

                    resolve();
                }
            )
        });
    }

    lista() {
        return new Promise((resolve, reject) => {
            this._db.all(
                'SELECT * FROM FATURAS',
                (erro, resultados) => {
                    if (erro) return reject('Não foi possível listar as faturass!');

                    return resolve(resultados);
                }
            )
        });
    }

    buscaPorId(id) {

        return new Promise((resolve, reject) => {
            this._db.get(
                `
                    SELECT *
                    FROM Faturas
                    WHERE idfatura = ?
                `,
                [id],
                (erro, fatura) => {
                    if (erro) {
                        return reject('Não foi possível encontrar a Fatura!');
                    }
                    return resolve(fatura);
                }
            );
        });
    }

    atualiza(fatura) {
        return new Promise((resolve, reject) => {
            this._db.run(`
                UPDATE FATURAS SET
                idusuario = ?,
                nome_empresa = ?,
                valor = ?,
                data_vencimento = ?,
                pagou = ?
                WHERE idfatura = ?
            `,
            [
                fatura.idusuario,
                fatura.nome_empresa,
                fatura.valor,
                fatura.data_vencimento,
                fatura.pagou,
                fatura.idfatura
            ],
            erro => {
                if (erro) {
                    return reject('Não foi possível atualizar a Fatura!');
                }

                resolve();
            });
        });
    }

    remove(id) {

        return new Promise((resolve, reject) => {
            this._db.get(
                `
                    DELETE 
                    FROM FATURAS
                    WHERE idfatura = ?
                `,
                [id],
                (erro) => {
                    if (erro) {
                        return reject('Não foi possível remover a Fatura!');
                    }
                    return resolve();
                }
            );
        });
    }
}

module.exports = FaturaDao;