const FaturaDao = require('../infra/fatura-dao');
const db = require('../../config/database');
const express = require('express');

module.exports = (app) => {
    app.get('/', function (req, resp) {
        resp.send(
            `
                <html>
                    <head>
                        <meta charset="utf-8">
                    </head>
                    <body>
                        <h1> Para consultas favor iniciar o angular com ng serve e verificar a porta 4200 </h1>
                    </body>
                </html>
            `
        );
    });

    app.get('/api/faturas', function (req, resp) {
        resp.header("Access-Control-Allow-Origin", "*");
        resp.header("Access-Control-Allow-Headers", "X-Requested-With");
        const faturaDao = new FaturaDao(db);
        faturaDao.lista()
            .then(faturas => {
                resp.json(faturas);
            }
            )
            .catch(erro => console.log(erro));
    });

    app.get('/api/faturas/form/:id', function (req, resp) {
        resp.header("Access-Control-Allow-Origin", "*");
        resp.header("Access-Control-Allow-Headers", "X-Requested-With");
        const faturaDao = new FaturaDao(db);
        const id = req.params.id;

        faturaDao.buscaPorId(id)
            .then(faturas => {
                resp.json(faturas);
            }
            )
            .catch(erro => console.log(erro));
    });

    app.post('/api/update', express.json({ type: '*/*' }), (req, resp) => {
        resp.header("Access-Control-Allow-Origin", "*");
        resp.header("Access-Control-Allow-Headers", "X-Requested-With");
        console.log(req.body);
        const faturaDao = new FaturaDao(db);
        if (req.body.idfatura> 0) {
            faturaDao.atualiza(req.body).then(faturas => {
                resp.json("Atualizado");
            }
            )
        }
        else {
            faturaDao.adiciona(req.body).then(faturas => {
                resp.json("Incluido!");
            })
        }
    })

    app.delete('/api/delete/:id', function (req, resp) {
        resp.header("Access-Control-Allow-Origin", "*");
        resp.header("Access-Control-Allow-Headers", "X-Requested-With");
        const id = req.params.id;
        const faturaDao = new FaturaDao(db);
        faturaDao.remove(id).then(faturas => {
            resp.json("excluido");
        }
        )
            .catch(erro => console.log(erro));
    });
};